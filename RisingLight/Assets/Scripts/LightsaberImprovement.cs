﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightsaberImprovement : MonoBehaviour
{
    //particle system
    [SerializeField]
    private GameObject[] increaseDamageVisual;

    //skript
    private Skilltree skilltree;

    void Start()
    {
        //Get the script
        skilltree = FindObjectOfType<Skilltree>();            
    }

    void Update()
    {
        // if the spell "IncreaseDamage" is activated the particle systems show up
       if(skilltree.VisualLightsaberIsActivated == true)
        {
            increaseDamageVisual[0].SetActive(true);
            increaseDamageVisual[1].SetActive(true);
            increaseDamageVisual[2].SetActive(true);
            increaseDamageVisual[3].SetActive(true);
            Destroy(this, 2f);
            
        }
    }
}
