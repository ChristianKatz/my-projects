﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Every soldier gets this script
public class EnemyControll : MonoBehaviour
{
    //the health bar of the enemy soldier
    [SerializeField]
    private Slider HealthBar;

    // the enemy damage
    private int enemyhealth = 500;
    public int EnemyHealth
    {
        get
        {
            return enemyhealth;
        }
        set
        {
            enemyhealth = value;
        }
    }
    // the player damage
    private int playerdamage = 10;
    public int PlayerDamage
    {
        get
        {
            return playerdamage;
        }
        set
        {
            playerdamage = value;
        }
    }
    // the damage of the "IncredibleSlap" spell
    private int incredibleSlapDamage = 0;
    public int IncredibleSlapDamage
    {
        get
        {
            return incredibleSlapDamage;
        }
        set
        {
             incredibleSlapDamage = value;
        }
    }

    // get the ScriptableObjects
    public ScriptableObjectMagicAbilities[] MagicAbility;

    void Update()
    {
        //the healtbar will be updated every time to get the current health
        HealthBar.value = enemyhealth; 

        // if the enemy health reached 0 the soldier will be destroyed
        if (enemyhealth <= 0)
        {
            Destroy(gameObject);
        }       
    }
    //if the spells or the sword of the player hits the enemy he will get damage

    private void OnTriggerEnter(Collider other)
    {
        for(int i = 0; i <= 2; i++)
        {
            if (other.CompareTag(MagicAbility[i].TriggerTag))
            {
                enemyhealth -= MagicAbility[i].magicDamage;
            }
        }
        if (other.CompareTag("PlayerTeam"))
        {          
            enemyhealth -= playerdamage;
            enemyhealth -= IncredibleSlapDamage;
        }

    }
}
