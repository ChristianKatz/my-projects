﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    // script, which is needed
    Currency currency;

    //Pause Menu, which can be set off and on
    [SerializeField]
    private GameObject Menu;

    //Scene names from the Main Menu and the Battleground
    private string SceneMenuName = "MainMenu";
    private string SceneName = "Play";

    //Bool variable, if the Menu is activated or not
    private bool MenuIsActivated = false;

    void Start()
    {
        // At the beginning of the game, the menu is set to off
        Menu.SetActive(false);

        // get the script
        currency = FindObjectOfType<Currency>();
    }

    void Update()
    {
        //This method is permanently activated because the player should be able to open the menu in the game every second
        MenuActivation();
    }

    // open and close the Menu
    // if the player opens the menu the game stops, the income stops and both will run again if he closes the menu
    void MenuActivation()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(MenuIsActivated == false)
            {               
                Menu.SetActive(true);
                Time.timeScale = 0;
                currency.StopMagicPoints = true;
                MenuIsActivated = true;
            }
            else
            {
                Menu.SetActive(false);
                Time.timeScale = 1;
                currency.StopMagicPoints = false;
                MenuIsActivated = false;
            }
        }
    }
    // button to restart the game
    public void Restart()
    {
        SceneManager.LoadScene(SceneName);
        Time.timeScale = 1;
    }
    // button to get to the Main Menu
    public void StartMenu()
    {
        Menu.SetActive(false);
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneMenuName);
    }
    //button to quit the game
    public void Quit()
    {
        Application.Quit();
    }

}
