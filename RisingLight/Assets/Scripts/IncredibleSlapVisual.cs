﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncredibleSlapVisual : MonoBehaviour
{
    // the particle system, what will show up, when the spell "Incredible Slap" is activated
    [SerializeField]
    private GameObject playerMagicBoost;

    //script 
    private Skilltree skilltree;

    void Start()
    {
        // get the scripts
        skilltree = FindObjectOfType<Skilltree>();
    }

    void Update()
    {
        //If the spell is activated the visual effect shows up
        if (skilltree.VisualIncredibleSlapIsActivated == true)
            playerMagicBoost.SetActive(true);

        //If the spell is deactivated the visual effect doesn't shows up
        if (skilltree.VisualIncredibleSlapIsActivated == false)
            playerMagicBoost.SetActive(false);

    }
}
