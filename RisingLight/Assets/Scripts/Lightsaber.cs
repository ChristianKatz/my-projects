﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//THIS SCRIPT IS NOT FROM ME  Source: https://www.youtube.com/watch?v=ZT8eutqzW5A&t=399s
public class Lightsaber : MonoBehaviour
{
    // line renderer for the lightsaber
    private LineRenderer lineRenderer;

    // start position of the laser
    [SerializeField]
    private Transform startPos;

    //end position of the laser
    [SerializeField]
    private Transform endPos;

    // the offset of the laser texture
    private float textureOfsset = 0f;

    // set on or off the lightsaber
    private bool on = true;

    // save the vectors of the end position of the laser
    private Vector3 endPosExtendedPosition;
    
    void Start()
    {
        // get the line renderer from the lightsaber
        lineRenderer = GetComponent<LineRenderer>();

        // get the local position of the end, which is relative to the parent 
        endPosExtendedPosition = endPos.localPosition;
    }

    void Update()
    {
        //turn lightsaber off and on
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if (on)
            {
                on = false;

            }
            else
            {
                on = true;
            }
        }
        //extend the line
        if(on)
        {
            endPos.localPosition = Vector3.Lerp(endPos.localPosition, endPosExtendedPosition, Time.deltaTime * 5f);
        }
        //hide line
        else
        {
            endPos.localPosition = Vector3.Lerp(endPos.localPosition, startPos.localPosition, Time.deltaTime * 5f);
        }
        // update line position
        lineRenderer.SetPosition(0, startPos.position);
        lineRenderer.SetPosition(1, endPos.position);

        //pan texture
        textureOfsset -= Time.deltaTime * 2f;
        if(textureOfsset < -10f)
        {
            textureOfsset += 10f;
        }
        lineRenderer.sharedMaterials[1].SetTextureOffset("_MainTex", new Vector2(textureOfsset, 0));
    }
}
