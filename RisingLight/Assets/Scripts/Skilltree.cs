﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class Skilltree : MonoBehaviour
{
    //Scripts, which are needed
    Currency currency;
     public EnemyControll[] enemyControll;

    //Get the ScriptableObjects
    public ScriptableObjectSwordUpdate[] SwordUpdate;
    public ScriptableObjectMagicAbilities[] MagicAbilitiy;

    //Enemy and Player count
    int counter = 6;

    //Variables for the Incredible Slap spell to show the particle system
    private bool visualIncredibleSlapIsActivated = false;
    public bool VisualIncredibleSlapIsActivated
    {
        get
        {
            return visualIncredibleSlapIsActivated;
        }
        set
        {
            visualIncredibleSlapIsActivated = value;
        }
    }

    //Variables for the lighsaber material to change the color and to show the color change
    [SerializeField]
    private Renderer lightsaberMaterials;
    private bool visualLighsaberIsActivated = false;
    public bool VisualLightsaberIsActivated
    {
        get
        {
            return visualLighsaberIsActivated;
        }
        set
        {
            visualLighsaberIsActivated = value;
        }
    }

    //Variables for the differnt menus
    [SerializeField]
    private GameObject mainMenuButtons;
    [SerializeField]
    private GameObject backButton;
    [SerializeField]
    private GameObject soldierUpgradesMenu;
    [SerializeField]
    private GameObject magicAbilitiesMenu;

    //Variables for the Information buttons
    [SerializeField]
    private GameObject incredibleSlapInformation;
    [SerializeField]
    private GameObject poisonInformation;
    [SerializeField]
    private GameObject increaseDamageInformation;
    [SerializeField]
    private GameObject fireballInformation;
    [SerializeField]
    private GameObject canonBurstInformation;
    [SerializeField]
    private GameObject powerGunInformation;

    //Variables for the Cooldowns
    [SerializeField]
    private Image fireBallCooldownVisual;
    [SerializeField]
    private Image canonBurstCooldownVisual;
    [SerializeField]
    private Image powerGunCooldownVisual;

    //Variables for the barriers of the buttons
    [SerializeField]
    private GameObject poisonBarrier;
    [SerializeField]
    private GameObject increaseDamageBarrier;
    [SerializeField]
    private GameObject incredibleSlapBarrier;


    //Variables for the Fireball
    [SerializeField]
    private Transform fireballPosition;
    private int fireballSpeed = 100;
    private int fireballFallSpeed = 200;
    [SerializeField]
    private Transform fireballExplosionPosition;
    private bool fireballIsActivated = false;
    private bool fireballIsFalling = false;
    private bool fireballIsMoveable = false;
    private float fireballCooldown;
    [SerializeField]
    private Transform fireballSpawnPosition;
    GameObject FireballPrefab;
    private int gizmosRadius = 5;
    private float fireballGroundDistance;
    
    //Variable for the poisen damage
    private int poisonDamage = 2;

    //Variables to controll the activation of the 3 spells
    private bool poisenIsActivated = false;
    private bool increaseDamageIsActivated = false;
    private bool incredibleHitIsActivated = false;

    //Variables for the Canon Burst
    private bool canonBurstIsActivated = false;
    private float canonBurstCooldown;
    [SerializeField]
    private PlayableDirector canonAnimation;

    //Variables for the Power Gun
    [SerializeField]
    private Transform powerGunCurrentPosition;
    [SerializeField]
    private Transform powerGunStartRotation;
    [SerializeField]
    private GameObject projectile;
    [SerializeField]
    private Transform shootPosition;
    [SerializeField]
    private int maxDistanceDeltaPowerGun;
    private int rotationSpeed = 60;
    private int shootPowerZ = 10000;
    private int shootPowerX = 40000;
    private int shootPowerY = -5000;
    GameObject projectilePrefab;
    private float powerGunCooldown;
    private bool powerGunIsActivated = false;
    private float powerGunUsedTime = 13;
    private bool gunMovesToStartPosition;
    private float clampX;
    float compensateShootDirection = 0;

    void Start()
    {
        //The lightsaber will be set to default color at the beginning of the game
        lightsaberMaterials.sharedMaterial.color = Color.blue;    
   
        //at the beginning the menus are set to off
        soldierUpgradesMenu.SetActive(false);
        magicAbilitiesMenu.SetActive(false);

        //get the script
        currency = FindObjectOfType<Currency>();

        // Get the Cooldown Informations from the ScriptableObjects
        powerGunCooldown = MagicAbilitiy[0].Cooldown;
        canonBurstCooldown = MagicAbilitiy[1].Cooldown;
        fireballCooldown = MagicAbilitiy[2].Cooldown;

        //All Information will be set to off at the beginning
        increaseDamageInformation.SetActive(false);
        poisonInformation.SetActive(false);
        incredibleSlapInformation.SetActive(false);
        powerGunInformation.SetActive(false);
        fireballInformation.SetActive(false);
        canonBurstInformation.SetActive(false);

        //All Barriers will be set to off at the beginning
        increaseDamageBarrier.SetActive(false);
        poisonBarrier.SetActive(false);
        incredibleSlapBarrier.SetActive(false);
    }

    void Update()
    {
        //Here is the display of the Fireball cooldown, the cooldown calculation and the permanent activation of the "Fireball" Method because of the if conditions
        Fireball();
        fireballCooldown -= Time.deltaTime;
        fireBallCooldownVisual.fillAmount = fireballCooldown / 10;

        //Here is the display of the Fireball cooldown and the cooldown calculation
        canonBurstCooldown -= Time.deltaTime;
        canonBurstCooldownVisual.fillAmount = canonBurstCooldown / 10;

        ////Here is the display of the Power Gun cooldown, the cooldown calculation and the permanent activation of the "PowerGun" Method because of the if conditions
        PowerGun();
        powerGunCooldown -= Time.deltaTime;
        powerGunCooldownVisual.fillAmount = powerGunCooldown / 10;

        //the enemy script has to be updated, because of the respawn of the soldiers
        enemyControll = FindObjectsOfType<EnemyControll>();

    }

    //With this Button the player is able to go back to the main menu of the skilltree
    public void GoBackButton()
    {
        backButton.SetActive(false);
        soldierUpgradesMenu.SetActive(false);
        mainMenuButtons.SetActive(true);
        magicAbilitiesMenu.SetActive(false);

    }

    // With this Button the player is able to go to the "SoldierUpgrades" menu
    public void SoldierUpdgradesMenuButton()
    {
        mainMenuButtons.SetActive(false);
        soldierUpgradesMenu.SetActive(true);
        backButton.SetActive(true);
    }

    // With this Button the player is able to go to the "MagicAbilities" menu
    public void MagicAbilitiesButton()
    {
        mainMenuButtons.SetActive(false);
        magicAbilitiesMenu.SetActive(true);
        backButton.SetActive(true);
    }

    //information Buttons show up and the Time Scale will set to 0 to stop the game
    public void InfoIncredibleSlap()
    {
        incredibleSlapInformation.SetActive(true);
        Time.timeScale = 0;
    }
    public void InfoIncreaseDamage()
    {
        increaseDamageInformation.SetActive(true);
        Time.timeScale = 0;
    }
    public void InfoPoison()
    {
        poisonInformation.SetActive(true);
        Time.timeScale = 0;
    }
    public void InfoFireball()
    {
        fireballInformation.SetActive(true);
        Time.timeScale = 0;
    }
    public void InfoCanonBurst()
    {
        canonBurstInformation.SetActive(true);
        Time.timeScale = 0;
    }
    public void InfoGunPower()
    {
        powerGunInformation.SetActive(true);
        Time.timeScale = 0;
    }

    // close the Info Window and activate the game again
    public void GoBackFromInfo()
    {
        incredibleSlapInformation.SetActive(false);
        poisonInformation.SetActive(false);
        increaseDamageInformation.SetActive(false);
        fireballInformation.SetActive(false);
        canonBurstInformation.SetActive(false);
        powerGunInformation.SetActive(false);
        Time.timeScale = 1;
    }

    //Poison will be activated and does damage over time
    //Before the Poisen can be used, the player has to have enough Magic Points
    //The spell is permanetly activated
    //if the spell is activated the lighsaber becomes green
    public void PoisenButton()
    {                 
        if (currency.MagicPoints >= SwordUpdate[0].cost && poisenIsActivated == false)
        {
            currency.MagicPoints -= SwordUpdate[0].cost;
            StartCoroutine(Poison());
            lightsaberMaterials.sharedMaterial.color = Color.green;
            poisonBarrier.SetActive(true);
            poisenIsActivated = true;

           
        }
    }
    IEnumerator Poison()
    {
        yield return new WaitForSeconds(SwordUpdate[0].DamageCooldownInSecond);

        for (int i = 0; i <= enemyControll.Length -1; i++)
        {
           // if(enemyControll[i] != null)
            enemyControll[i].EnemyHealth -= SwordUpdate[0].damage;
        }
        StartCoroutine(Poison());
    }

    //The damage of the soldiers will be increased 
    //The spell is permanetly activated
    //if the spell is activated a particle system will appear at the lightsaber 
    public void IncreaseSwordDamageButton()
    {
        if (currency.MagicPoints >= SwordUpdate[1].cost && increaseDamageIsActivated == false)
        {                      
            currency.MagicPoints -= SwordUpdate[1].cost;
            StartCoroutine(IncreaseSwordDamage());
            increaseDamageBarrier.SetActive(true);
            increaseDamageIsActivated = true;
        }
    }
    IEnumerator IncreaseSwordDamage()
    {
        yield return new WaitForSeconds(SwordUpdate[0].DamageCooldownInSecond);
 
        for (int i = 0; i <= enemyControll.Length - 1; i++)
        {
            enemyControll[i].PlayerDamage = SwordUpdate[1].damage;          
            visualLighsaberIsActivated = true;           
        }
        StartCoroutine(IncreaseSwordDamage());

    }

    //The spell is permanently activated
    //has time intervals to do damage
    //the spell will be automatically activated again after a fixed time and increase the damage of the soldiers for 5 seconds
    //if the damage is increadsed a particle system will appear in the head of the soldier
    public void IncredibleSlapButton()
    {
       
        if (currency.MagicPoints >= SwordUpdate[2].cost && incredibleHitIsActivated == false)
        {
           
            currency.MagicPoints -= SwordUpdate[2].cost;
            StartCoroutine(IncredibleSlap());
            incredibleSlapBarrier.SetActive(true);
            incredibleHitIsActivated = true;
        }
        
    }
    IEnumerator IncredibleSlap()
    {

        visualIncredibleSlapIsActivated = true;
        
        if(visualIncredibleSlapIsActivated == true)
        {
            for (int i = 0; i <= enemyControll.Length - 1; i++)
            {
                 enemyControll[i].IncredibleSlapDamage = SwordUpdate[2].damage;
            }
        }
        yield return new WaitForSeconds(5f);
            for (int i = 0; i <= enemyControll.Length - 1; i++)
            {
                enemyControll[i].IncredibleSlapDamage = 0;
            }
        visualIncredibleSlapIsActivated = false;
        yield return new WaitForSeconds(SwordUpdate[2].DamageCooldownInSecond);
        StartCoroutine(IncredibleSlap());
       

    }

    // the player is able to control the Fireball with WASD and Q for the fall
    // he can use the spell, when he get the amount of Magic Points to buy it and when the cooldown reached 0 again
    // it's a AOE spell with hit up to 3 enemies
    public void FireballButton()
    {
        if (currency.MagicPoints >= MagicAbilitiy[2].cost && fireballCooldown <=0  && fireballIsActivated == false)
        {           
            FireballPrefab = Instantiate(fireballPosition.gameObject, fireballSpawnPosition.position, Quaternion.identity);         
            fireballIsActivated = true;
            fireballIsMoveable = true;
            currency.MagicPoints -= MagicAbilitiy[2].cost;
            fireballCooldown = MagicAbilitiy[2].Cooldown;

        }
    }
    public void Fireball()
    {
        if (fireballIsMoveable == true)
        {
            float moveHorizontal = Input.GetAxis("Horizontal") * Time.deltaTime * fireballSpeed;
            float moveVertical = Input.GetAxis("Vertical") * Time.deltaTime * fireballSpeed;
            FireballPrefab.transform.Translate(new Vector3(moveHorizontal, 0, moveVertical));
        }
        if (Input.GetKey("q") && fireballIsMoveable == true)
        {
            fireballIsMoveable = false;
            fireballIsFalling = true;
        }
        if (fireballIsFalling == true && fireballPosition != null)
        {
            Vector3 fireballDestination = Vector3.MoveTowards(FireballPrefab.transform.position, fireballExplosionPosition.position, Time.deltaTime * fireballFallSpeed);
            FireballPrefab.transform.position = fireballDestination;

            if (FireballPrefab.transform.position == fireballExplosionPosition.position)
            {
                fireballIsActivated = false;
                fireballIsFalling = false;               
                Destroy(FireballPrefab, 1);
                //Explosion
            }
        }
        if (FireballPrefab != null)
        {
            fireballExplosionPosition.position = FireballPrefab.transform.position + new Vector3(0, -FireballPrefab.transform.position.y, 0);
        }
    }

    //If the player buys the spell "Canon Burst" a Animation will be played and fire on the enemy one time
    //he can buy the spell,. if he has the amount of Magic Points to buy it
    //its a AOE spell, which hit the first two enemies of the right and left side
    public void CanonBurst()
    {
        if (currency.MagicPoints >= MagicAbilitiy[1].cost && canonBurstCooldown <= 0)
        {
            currency.MagicPoints -= MagicAbilitiy[1].cost;
            canonAnimation.Play();
            canonBurstCooldown = MagicAbilitiy[1].Cooldown;
        }
    }

    //It's a controlable Gun for the player. The movement is with the mouse and he can shoot with the right mouse button
    //The player can shoot for 10 seconds, if the spell is activated
    //he can use the spell, if he has the amount of Magic Points and the colldown reached 0 again
    //after using the gun it will be automatically rotated to the start point back
    //the gun shoots with bullets
    public void PowerGunButton()
    {
        if (currency.MagicPoints >= MagicAbilitiy[0].cost && powerGunCooldown <= 0)
        {
            currency.MagicPoints -= MagicAbilitiy[0].cost;          
            powerGunIsActivated = true;
            powerGunCooldown = MagicAbilitiy[0].Cooldown;
        }
    }
    public void PowerGun()
    {
        if (powerGunIsActivated == true)
        {
                       
            float mouseX = Input.GetAxis("Mouse X") * Time.deltaTime * rotationSpeed;
            powerGunCurrentPosition.Rotate(new Vector3(0, mouseX, 0));
          
            clampX += mouseX;        
            clampX = Mathf.Clamp(clampX, -30f, 30f);
            Quaternion ClampAngle = Quaternion.Euler(27.056f, clampX, 0);
            powerGunCurrentPosition.transform.rotation = ClampAngle;
                     
            powerGunUsedTime -= Time.deltaTime;

            compensateShootDirection = powerGunCurrentPosition.rotation.y * shootPowerX;

            if (Input.GetMouseButtonDown(1))
            {
                projectilePrefab = Instantiate(projectile, shootPosition.position, Quaternion.identity);
                projectilePrefab.GetComponent<Rigidbody>().AddForce(new Vector3(compensateShootDirection, shootPowerY, shootPowerZ));
                Destroy(projectilePrefab, 3f);
            }

            if (powerGunUsedTime <= 0)
            {
                gunMovesToStartPosition = true;
                powerGunIsActivated = false;
                powerGunUsedTime = 10;
            }
        }
        if (gunMovesToStartPosition == true)
        {           
            powerGunCurrentPosition.transform.rotation = Quaternion.RotateTowards(powerGunCurrentPosition.transform.rotation, powerGunStartRotation.transform.rotation, maxDistanceDeltaPowerGun * Time.deltaTime);
        }
    }
}
