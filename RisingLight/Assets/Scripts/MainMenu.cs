﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // the name of the Scene, which is the battleground
    private string SceneName = "Play";

    //close the Game
    public void EndGame()
    {
        Application.Quit();
    }

    // Load the "Play" Scene, which is the battleground
    public void StartGame()
    {
        SceneManager.LoadScene(SceneName);
    }
}
