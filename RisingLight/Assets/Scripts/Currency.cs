﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Currency : MonoBehaviour
{
    // the time how long the player and the enemy have time to kill soldiers
    private float time = 300;

    // get the scripts
    private FightDestination fightDestination;
    public ScriptableObjectCurrency[] WageUpdate;

    // display the current time, currency and the different wins
    [SerializeField]
    private TextMeshProUGUI playTime;
    [SerializeField]
    private TextMeshProUGUI enemyWins;
    [SerializeField]
    private TextMeshProUGUI playerWins;
    [SerializeField]
    private TextMeshProUGUI draw;
    [SerializeField]
    private TextMeshProUGUI currency;

    //Varibale to count the current magic points
    private int magicPoints;
    public int MagicPoints
    {
        get
        {
            return magicPoints;
        }
        set
        {
            magicPoints = value;
        }
    }

    // Variables, which activate the currency increase
    bool boughtSaleEduction = false;
    bool boughtVehicles = false;
    bool boughtMerchant = false;

    //Variables, which lock the spell for using it again
    bool MerchantIsActivated = false;
    bool SaleEducationIsActivated = false;
    bool VehicleIsActivated = false;

    // Variable, which stops the Magic Points income, if the player uses the pause menu
    bool stopMagicPoints = false;
    public bool StopMagicPoints
    {
        get
        {
            return stopMagicPoints;
        }
        set
        {
            stopMagicPoints = value;
        }
    }

    // the 3 visual Barrier of the buttons
    [SerializeField]
    private GameObject vehicleBarrier;
    [SerializeField]
    private GameObject merchantBarrier;
    [SerializeField]
    private GameObject saleEducationBarrier;

    void Start()
    {
        // at the beginning of the game the difrrent win displays are set to off
        enemyWins.gameObject.SetActive(false);
        playerWins.gameObject.SetActive(false);
        draw.gameObject.SetActive(false);

        // the script has to be found
        fightDestination = FindObjectOfType<FightDestination>();

        // At the beginning of the game the 3 visual Barrier of the buttons are deaktivated
        vehicleBarrier.SetActive(false);
        saleEducationBarrier.SetActive(false);
        merchantBarrier.SetActive(false);
    }

    void Update()
    {
        //calculation of the time and the conversion in the text variable
        time -= Time.deltaTime;
        playTime.text = string.Format("{0:0}", time);

        // if the time reached 0 the kills of the enemy and the player will be compared to display, who has won
        if(time <= 0)
        {
            Time.timeScale = 0;

            if(fightDestination.PlayerKills < fightDestination.EnemyKills)
            {
                enemyWins.gameObject.SetActive(true);
            }
            if (fightDestination.PlayerKills > fightDestination.EnemyKills)
            {
                playerWins.gameObject.SetActive(true);
            }
            if(fightDestination.PlayerKills == fightDestination.EnemyKills)
            {
                draw.gameObject.SetActive(true);
            }
        }

        //the conversion of the Magic Points in a text variable
        currency.text = string.Format("Magic Points: " + magicPoints);
    }

    // Click button to get Magic Points
    public void ClickForMagicPoints()
    {
        if(stopMagicPoints == false)
        magicPoints++;
    }

    // If the player has enough Magic points to buy the Upgrade the passiv income will be increased
    public void Merchant()
    {
        if(magicPoints >= WageUpdate[0].cost && MerchantIsActivated == false)
        {
            magicPoints -= WageUpdate[0].cost;
            StartCoroutine(RaiseMoneyIncome());
            boughtMerchant = true;
            merchantBarrier.SetActive(true);
            MerchantIsActivated = true;        
        }       
    }

    // If the player has enough Magic points to buy the Upgrade the passiv income will be increased
    public void SaleEducation()
    {      
        if (magicPoints >= WageUpdate[1].cost && SaleEducationIsActivated == false)
        {
            magicPoints -= WageUpdate[1].cost;
            StartCoroutine(RaiseMoneyIncome());
            boughtSaleEduction = true;
            saleEducationBarrier.SetActive(true);
            SaleEducationIsActivated = true;                      
        }
    }

    // If the player has enough Magic points to buy the Upgrade the passiv income will be increased
    public void Vehicle()
    {
        if (magicPoints >= WageUpdate[2].cost && VehicleIsActivated == false)
        {
            magicPoints -= WageUpdate[2].cost;
            StartCoroutine(RaiseMoneyIncome());
            boughtVehicles = true;
            vehicleBarrier.SetActive(true);
            VehicleIsActivated = true;          
        }
    }

    // that's the Enumerator, which increase the passive income every second
    IEnumerator RaiseMoneyIncome()
    {
        if(stopMagicPoints == false)
        {
            if (boughtMerchant)
            {
                yield return new WaitForSeconds(WageUpdate[0].WageCooldownInSeconds);
                magicPoints += WageUpdate[0].Wage;
            }

            if (boughtSaleEduction)
            {
                    yield return new WaitForSeconds(WageUpdate[1].WageCooldownInSeconds);
                    magicPoints += WageUpdate[1].Wage;
            }

            if (boughtVehicles)
            {
                    yield return new WaitForSeconds(WageUpdate[2].WageCooldownInSeconds);
                    magicPoints += WageUpdate[2].Wage;
            }

            StartCoroutine(RaiseMoneyIncome());
        }
    }
}
