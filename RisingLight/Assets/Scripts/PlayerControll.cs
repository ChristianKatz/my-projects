﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

//Every soldier gets this script
public class PlayerControll : MonoBehaviour
{

    //the health bar of the player soldier
    [SerializeField]
    private Slider HealthBarPlayer;

    // the player health
    private int playerHealth = 500;
    public int PlayerHealth
    {
        get
        {
            return playerHealth;
        }
        set
        {
            playerHealth = value;
        }
    }
    //the enemy damage
    private int enemydamage = 100;

    void Update()
    {
        // if the player health reached 0 the soldier will be destroyed
        if (playerHealth <= 0)
        {
            Destroy(gameObject);
        }
    }
    //if the sword of the enemy hits the player he will get damage
    //the healtbar will be updated every time to get the current health
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("EnemyTeam"))
        {
            playerHealth -= enemydamage;
            HealthBarPlayer.value = playerHealth;
        }          
    }

}
