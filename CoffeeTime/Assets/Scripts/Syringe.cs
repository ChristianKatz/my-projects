﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class Syringe : MonoBehaviour
    {
        // To destroy the syringe again, I need the object
        [SerializeField]
        private GameObject Boost;

        // In order to influence the speed of the player, I need the script "PlayerMovement"
        private PlayerMovement Player;

        // disable sprite renderer and collider after collecting
        // I do that because the player is not allowed to collect the sprite again
        private SpriteRenderer SyringeSprite;
        private Collider2D SyringeCollider;

        [SerializeField]
        private GameObject HighFace;
        [SerializeField]
        private GameObject NormalFace;

        private void Start()
        {
            //Scripts and components are called
            Player = FindObjectOfType<PlayerMovement>();
            SyringeSprite = GetComponent<SpriteRenderer>();
            SyringeCollider = GetComponent<Collider2D>();
        }

        // When the player touches the collider the player will get a boost
        //collider and sprite renderer are disabled and the coroutine will be started 
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                Player.runSpeed = 15;
                Player.jumpForce = 14;
                SyringeSprite.enabled = false;
                SyringeCollider.enabled = false;
                HighFace.SetActive(true);
                NormalFace.SetActive(false);
                StartCoroutine(deactivateBoost());
            }
        }

        // Cooldown for the syringe, after which the player gets his default values back and the syringe is removed from the game
        IEnumerator deactivateBoost()
        {
            yield return new WaitForSeconds(10f);
            Player.runSpeed = 10;
            Player.jumpForce = 12;
            HighFace.SetActive(false);
            NormalFace.SetActive(true);
            Destroy(Boost);
        }
    }
}
