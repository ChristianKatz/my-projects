﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class DestroyWaterdrops : MonoBehaviour
    {
        // to destroy the water drops on the umbrella I need access to the "Umbrella" script
        private Umbrella destroyUmbrella;

        void Start()
        {
            //get the script
            destroyUmbrella = FindObjectOfType<Umbrella>();
        }

        // at the start of the game, the umbrella is deactivated
        // The umbrella is activated when the player collects the sprite and presses the f key
        // The coroutine starts and activates the cooldown
        private void OnEnable()
        {
            StartCoroutine(DestroyUmbrella());
        }

        // using the trigger to destroy the water drops, if they hit the umbrella 
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Waterdrops"))
            {
                Destroy(other.gameObject);
            }
        }

        // that is the cooldown and shows how long the umbrella is activated 
        // after that time the umbrella will be destroyed
        IEnumerator DestroyUmbrella()
        {
            yield return new WaitForSeconds(10f);
            destroyUmbrella.Shield.SetActive(false);
        }
    }
}
