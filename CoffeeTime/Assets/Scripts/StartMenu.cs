﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoLA
{
    public class StartMenu : MonoBehaviour
    {
        // Scene names for the game Start, Highscore and Level Selection
        [SerializeField]
        private string SceneNameForPlay;
        [SerializeField]
        private string SceneNameForFirstLevel;
        [SerializeField]
        private string SceneNameForSecondLevel;
        [SerializeField]
        private string SceneNameForThirdLevel;
        [SerializeField]
        private string ScenenameForHighscoreMenu;
        [SerializeField]
        private string ScenenameForBack;

        // Game object containing the level selection
        [SerializeField]
        private GameObject activateLevelSelection;

        // GameObjekt, which puts the first page offline, if you want to go into the level selection
        [SerializeField]
        private GameObject SetFirstPageOff;


        private void Start()
        {
            // At the start of the menu the level selection is set offline
            activateLevelSelection.SetActive(false);
        }

        // Method for a button that starts the game
        public void Play()
        {
            SceneManager.LoadScene(SceneNameForPlay);
        }

        // Method for a button that ends the application
        public void Quit()
        {
            Application.Quit();
        }

        // Method for a button that turns off the first menu page and activates the second one with the level selection
        public void LevelSelection()
        {
            activateLevelSelection.SetActive(true);
            SetFirstPageOff.SetActive(false);
        }

        // methods, for the button, which starts the corresponding 3 levels
        public void FirstLevel()
        {
            SceneManager.LoadScene(SceneNameForFirstLevel);
        }
        public void SecondLevel()
        {
            SceneManager.LoadScene(SceneNameForSecondLevel);
        }
        public void ThirdLevel()
        {
            SceneManager.LoadScene(SceneNameForThirdLevel);
        }

        // method, for a button that calls the menu for the high score
        public void HighscoreMenu()
        {
            SceneManager.LoadScene(ScenenameForHighscoreMenu);
        }

        // method, for a button that leads from the level selection back to the first menu page
        public void Back()
        {
            SceneManager.LoadScene(ScenenameForBack);
        }
    }
}
