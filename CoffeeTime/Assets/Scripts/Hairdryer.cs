﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class Hairdryer : MonoBehaviour
    {
        // a GameObject to destroy it after picking up the hairdryer
        [SerializeField]
        private GameObject Lifebringer;

        // i need this script to modify the health of the player
        private PlayerMovement Player;

        void Start()
        {
            //calling up the script
            Player = FindObjectOfType<PlayerMovement>();
        }

        // using of triggers to increase the player's health when he touches the hairdryer
        // after that, the GameObject is destroyed
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                Player.Health++;
                Destroy(Lifebringer);
            }
        }
    }
}  
