﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

namespace CoATwoLA
{
    public class FinishMenu : MonoBehaviour
    {
        // string value for the scenen name 
        [SerializeField]
        private string ScenenameForMenu;

        // string value for the display of the coffee beans 
        [SerializeField]
        private TextMeshProUGUI Coffeebeans;

        // string value for the display of the time
        [SerializeField]
        private TextMeshProUGUI Time;

        // int and float values, that count the coffee beans and the time
        private int CoffeebeansScore;
        private float TimeScore;

        void Start()
        {
            // call the saved variables for the Highscore
            CoffeebeansScore = PlayerPrefs.GetInt("Coffeebeans", 0);
            TimeScore = PlayerPrefs.GetFloat("Time", 0);

            // The Cursor should be moveable and visible to use the menu 
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        void Update()
        {
            // The Highscore of the coffee beans and the time will be displayed on the screen
            Coffeebeans.text = string.Format("Coffeebeans: {0:0}", CoffeebeansScore);
            Time.text = string.Format("Time: {0:0}", TimeScore);
        }

        // Method for a button that leads back to the menu
        public void BackToTheMenu()
        {
            SceneManager.LoadScene(ScenenameForMenu);
        }

        // method fur a button, that retarts the level
        public void Again()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        }

        // method for a button, that starts the next level 
        public void NextLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

    }
}
