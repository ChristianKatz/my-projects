﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

namespace CoATwoLA
{
    public class FinishScreen : UIControll
    {
        // Text that is necessary for displaying the high score of the coffee beans
        [SerializeField]
        private TextMeshProUGUI Coffeebeans;

        //Text that is necessary for displaying the highscores of the time
        [SerializeField]
        private TextMeshProUGUI Time;

        //Int value that counts the coffee bean score
        private int CoffeebeansScore;

        //Float value that counts the time
        private float TimeScore;

        //string value for the scene name
        [SerializeField]
        private string ScenenameForMenu;


        void Start()
        {
            // Calling up the saved high score of coffee beans and the time
            CoffeebeansScore = PlayerPrefs.GetInt("Coffeebeans", 0);
            TimeScore = PlayerPrefs.GetFloat("Time", 0);

            // Cursor is not locked anymore and is visible so that the menu can be used
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        void Update()
        {
            // show the highscore
            Coffeebeans.text = string.Format("Coffeebeans: {0:0}", CoffeebeansScore);
            Time.text = string.Format("Time: {0:0}", TimeScore);
        }

        //Method for a button that clears the saved score
        public void ResetScore()
        {
            PlayerPrefs.DeleteKey("Coffeebeans");
            PlayerPrefs.DeleteKey("Time");
        }

        // Method for a button that leads back to the menu
        public void BackToTheMenu()
        {
            SceneManager.LoadScene(ScenenameForMenu);
        }
    }
}

