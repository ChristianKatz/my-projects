﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
   public class CoinControll : MonoBehaviour
   {       
    // Variable that destroys the coffee beans after the player hits it
    [SerializeField]
    private GameObject Coffeebean;

    //need this script to increase the score value
    private UIControll uIControll;

    void Start()
    {     
        // get the script
        uIControll = FindObjectOfType<UIControll>();
    }

    // If the player hits the coffee beans, the score will be increased by 1
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            uIControll.CurrentScore += 1;
            Destroy(Coffeebean);                     
        }
    }

   }
}
