﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class PlayerMovement : MonoBehaviour
    {
        // Int value for the player's running speed
        public int runSpeed;

        // Int value for the bounce
        public int jumpForce;

        // Float Value for the x-axis values that can be counted from -1 to 1
        private float moveHorizontal;

        // Int value to indicate the number of extra jumps left
        private int extraJump;

        // Rigidbody for influencing the player physics
        private Rigidbody2D rb;
        public Rigidbody2D RB
        {
            get
            {
                return rb;
            }
            set
            {
                rb = value;
            }
        }

        //Bool value to see if the player is moving to the right or leftt
        private bool facingRight = true;

        // GameObject that calls the menu when the player dies
        [SerializeField]
        private GameObject DeadMenu;

        // Bool value that shows if the player is on the ground or not
        private bool isGrounded;

        // Transform, which has the location if the player is on the ground or not
        [SerializeField]
        private Transform groundCheck;

        // float value that specifies the radius for checking the ground
        [SerializeField]
        private float checkRadius;

        // LayerMask, which indicates on which layer the ground should be checked
        [SerializeField]
        private LayerMask WhatIsGrounded;

        // Animator to run the animations
        [SerializeField]
        private Animator animator;

        // Int value that determines lives
        private int health = 4;
        public int Health
        {
            get
            {
                return health;
            }
            set
            {
                health = value;
            }
        }

        void Start()
        {
            // Call the rigidbody
            rb = GetComponent<Rigidbody2D>();

            // The time should be reset safely, so the game runs smoothly again
            Time.timeScale = 1;

            // When the game starts, the "DeadMenu" should be closed until the player dies
            DeadMenu.SetActive(false);

            // Cursor is locked and invisible, becasue it is irrelevant to gameplay
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        void Update()
        {
            // The run animation is executed at a value over 0
            animator.SetFloat("speed", Mathf.Abs(moveHorizontal));

            // It will be checked whether the player is on the ground or not
            isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, WhatIsGrounded);

            // If the player is on the ground, he can jump a total of 2 times
            if (isGrounded == true)
            {
                extraJump = 1;

                // If the player presses W he should be able to jump
                // With the Bool value in FixedUpdate the Rigidbody2D should be moved up
                // Because he is only allowed to jump twice in a row, the extraJump value should be reduced by 1
                // If he jumps the animation should be activated
                if (Input.GetKeyDown(KeyCode.W) && extraJump > 0)
                {
                    animator.SetBool("IsJumping", true);                    
                    extraJump--;
                    rb.velocity = Vector2.up * jumpForce;
                }
                else
                {
                    animator.SetBool("IsJumping", false);
                    
                }
            }

            else
            {
                if (Input.GetKeyDown(KeyCode.W) && extraJump > 0)
                {
                    animator.SetBool("IsJumping", true);                    
                    extraJump--;
                    rb.velocity = Vector2.up * jumpForce;
                }
            }

            // When the player reaches 0 lives, lives remain at zero and the player becomes immobilized
            // This opens the menu
            if (Health <= 0)
            {
                Health = 0;
                DeadMenu.SetActive(true);
            }

            // The player is not allowed to have more than 4 lives
            // If it should go beyond that, the value remains 4      
            if (Health > 4)
            {
                Health = 4;
            }

        }

        private void FixedUpdate()
        {
            // The player should be able to move with the A and D keys on the X-axis
            // The Rigidbody is moved, so FixedUpdate is needed too
            moveHorizontal = Input.GetAxis("Horizontal");
            rb.velocity = new Vector2(moveHorizontal * runSpeed, rb.velocity.y);

            // If the player moves to the left and the value of "moveHorizontal" is higher than 0, the sprite should be flipped
            // Thus the character looks in the right direction of movement
            if (facingRight == false && moveHorizontal > 0)
            {
                Flip();
            }

            // If the player moves to the right and the value of "moveHorizontal" is less than 0, the sprite should be flipped
            // Thus the character looks in the right direction of movement
            if (facingRight == true && moveHorizontal < 0)
            {
                Flip();
            }
        }

        // Method that flips the player's sprite when moving to the right or left
        void Flip()
        {
            facingRight = !facingRight;
            Vector2 Scaler = transform.localScale;
            Scaler.x *= -1;
            transform.localScale = Scaler;
        }
    }
}
