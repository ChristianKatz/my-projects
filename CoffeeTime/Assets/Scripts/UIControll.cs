﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace CoATwoLA
{
    public class UIControll : MonoBehaviour
    {
        // Text Variables to display the time, the score and the lives
        [SerializeField]
        private TextMeshProUGUI TimeText;
        [SerializeField]
        private TextMeshProUGUI HealthText;
        [SerializeField]
        private TextMeshProUGUI ScoreText;

        // I need the script to show the current life of the player
        private PlayerMovement playerMovement;

        // float value to display the time in seconds
        private float currentTime;

        // variable to count the current score
        // Property to use in another script because the score changes when the coffee beans are collected
        private int currentScore;
        public int CurrentScore
        {
            get
            {
                return currentScore;
            }
            set
            {
                currentScore = value;
            }
        }

        void Start()
        {
            // call the script
            playerMovement = FindObjectOfType<PlayerMovement>();
        }

        void Update()
        {
            // The time should be displayed in seconds in the game
            currentTime = Time.timeSinceLevelLoad;
            TimeText.text = string.Format("Time: {0:0}", currentTime);

            //display of lives and scores
            ScoreText.text = string.Format("Score: {0:0}", currentScore);
            HealthText.text = string.Format(playerMovement.Health + " x");

            // Save the score and time for the high score
            PlayerPrefs.SetInt("Coffeebeans", CurrentScore);
            PlayerPrefs.SetFloat("Time", currentTime);
            PlayerPrefs.Save();

        }
    }
}
