﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class Espresso : MonoBehaviour
    {
        // to destroy the espresso I need the GameObject
        [SerializeField]
        private GameObject Coffee;

        // to modify the movement of the palyer I need the "PlayerMovement" script
        private PlayerMovement Player;

        // Disable the sprite renderer and colliders after collecting the sprite
        // I do that because the player is not allowed to collect the sprite again
        private SpriteRenderer EspressoSprite;
        private BoxCollider2D EspressoCollider;

        // the GameObjects for the changing of the character faces
        [SerializeField]
        private GameObject HighFace;
        [SerializeField]
        private GameObject NormalFace;

        void Start()
        {
            // get the scripts and components       
            Player = FindObjectOfType<PlayerMovement>();
            EspressoSprite = GetComponent<SpriteRenderer>();
            EspressoCollider = GetComponent<BoxCollider2D>();

            // Highface is deactivated
            HighFace.SetActive(false);
        }
        // When the player touches the collider, the environment slows down, but the player can move as fast as before
        // the collider and the sprite renderer will be deactivated and the coroutine will be started
        // the HighFace will be activated and the NormalFace will be deactivated
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                Time.timeScale = 0.5f;
                Time.fixedDeltaTime = Time.timeScale * 0.02f;
                Player.runSpeed = 22;
                Player.jumpForce = 22;
                Player.RB.gravityScale = 7;
                EspressoSprite.enabled = false;
                EspressoCollider.enabled = false;
                HighFace.SetActive(true);
                NormalFace.SetActive(false);
                StartCoroutine(SlowMotion());
            }
        }

        // this is the slow motion cooldown, after which the environment has returned to its previous speed
        // the espresso and this script will be deleted 
        // Normal Face will be activated and the HighFace will be deactivated 
        IEnumerator SlowMotion()
        {
            yield return new WaitForSeconds(5f);
            Time.timeScale = 1;
            Player.runSpeed = 10;
            Player.jumpForce = 12;
            Player.RB.gravityScale = 2;
            HighFace.SetActive(false);
            NormalFace.SetActive(true);
            Destroy(Coffee);
        }
    }
}
