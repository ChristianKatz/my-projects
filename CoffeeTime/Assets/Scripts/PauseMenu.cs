﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoLA
{
    public class PauseMenu : MonoBehaviour
    {
        // GameObject for the menu, so it can be turned on and off
        [SerializeField]
        private GameObject Pause;

        // String value for the scene name
        [SerializeField]
        private string ScenenameForMenu;

        void Start()
        {
            // When the game starts, the menu should be off
            Pause.SetActive(false);
        }

        void Update()
        {
            // If the player wants to open the menu, he should press the Escape key
            // The cursor is activated and movable
            // Furthermore, the time should stop, so that the player gets no damage and the time is not falsified
            if (Input.GetKey(KeyCode.Escape))
            {
                Pause.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                Time.timeScale = 0;
            }
        }

        // Method for a button that leads back to the menu
        // If the button is pressed, the time should be reactivated
        public void BackToTheMainMenu()
        {
            SceneManager.LoadScene(ScenenameForMenu);
            Time.timeScale = 1;
        }

        // Method for a button leading back to game
        // If the button is pressed, the time should be reactivated
        public void Resume()
        {
            Pause.SetActive(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1;
        }

        // Method for a button with which the player can leave the game
        // If the button is pressed, the time should be reactivated
        public void Quit()
        {
            Application.Quit();
        }
    }
}
