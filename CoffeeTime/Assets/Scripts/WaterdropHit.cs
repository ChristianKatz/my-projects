﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class WaterdropHit : MonoBehaviour
    {
        // Need this script to deducted the life by 1 
        private PlayerMovement Player;

        // Need this cript of the spawning water drops to spawn a new water drop 
        private SpawningEnemy spawning;

        void Start()
        {
            // Both scripts are called
            Player = FindObjectOfType<PlayerMovement>();
            spawning = GetComponentInParent<SpawningEnemy>();
        }

        // using of triggers so that the player gets life deducted on a touch
        // If the player touches a raindrop, the GameObject is destroyed and a new one will be spawned
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                Destroy(spawning.SpawnedObject);
                Player.Health--;
                spawning.SpawnedObject = Instantiate(spawning.Enemy[Random.Range(0, 2)], spawning.Location);
            }
        }
    }
}
