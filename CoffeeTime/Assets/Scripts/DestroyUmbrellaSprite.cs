﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class DestroyUmbrellaSprite : MonoBehaviour
    {
        // to destroy the umbrella sprite I need the gameobject
        [SerializeField]
        private GameObject UmbrellaObject;

        // This bool value will send it to the "Umbrella" script, thus the Umbrella can be activated
        private bool destroyedSprite;
        public bool DestroyedSprite
        {
            get
            {
                return destroyedSprite;
            }
            set
            {
                destroyedSprite = value;
            }
        }

        void Start()
        {
            // the bool value is set to false because the default value is true and shouldn't destroy the umbrella sprite immediately
            DestroyedSprite = false;
        }

        // using the trigger to destroy the sprite, thus the umbrella can be activated
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                DestroyedSprite = true;
                Destroy(UmbrellaObject);
            }
        }
    }
}
 