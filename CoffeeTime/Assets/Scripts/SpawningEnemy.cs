﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class SpawningEnemy : MonoBehaviour
    {
        //For the spawning water drops I need the 3 different models and the location where they should spawn
        public GameObject[] Enemy;
        public Transform Location;

        // The speed of the falling drops of water
        public int speed;

        // Game object for the spawn object and location
        // The property is needed to use the variable for another script if the Acces Modifier is Private
        private GameObject spawnedObject;
        public GameObject SpawnedObject
        {
            get
            {
                return spawnedObject;
            }
            set
            {
                spawnedObject = value;
            }
        }

        // Int Value that causes raindrops to be destroyed if they do not hit the player and fall outside the map
        public int Destructionvalue;

        void Update()
        {
            // The drops of water get a random spawning position along the X-axis
            Enemy[Random.Range(0, 2)].transform.position = new Vector2(Random.Range(-4, 4), transform.position.y);

            // If no object has been spawned, spawn one of three different ones along the x-axis
            if (spawnedObject == null)
            {
                spawnedObject = Instantiate(Enemy[Random.Range(0, 2)], Location);
            }

            // If the Raindrop does not hit the player, it will be destroyed at a certain Y-axis value
            // Then a new Raindrop is spawned
            if (Destructionvalue > spawnedObject.transform.position.y)
            {
                Destroy(spawnedObject);
                spawnedObject = Instantiate(Enemy[Random.Range(0, 2)], Location);
            }

            // If an object has been spawned, it will be accelerated down
            spawnedObject.transform.Translate(new Vector2(0, -speed * Time.deltaTime));
        }

    }
}

