﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class Umbrella : MonoBehaviour
    {
        // I need the game object to activate the Umbrella after pressing the F key
        public GameObject Shield;

        // The game object "PressF" is necessary for displaying that the umbrella can be used
        [SerializeField]
        private GameObject PressF;

        // The script "DestroyUmbrella" is for confirming that the player has collected the umbrella and thus can be activated
        private DestroyUmbrellaSprite DestroyUmbrella;

        void Start()
        {
            // call the script
            DestroyUmbrella = FindObjectOfType<DestroyUmbrellaSprite>();

            //the default bool value is true, I have to set it to false because the umbrella should not be activated after the game starts
            PressF.SetActive(false);
            Shield.SetActive(false);

        }
        private void Update()
        {
            // if the player has collected the umbrella, he is able to see, that he can use it 
            // After pressing the f key the umbrella is activated 
            // after the activation the f button woll be deactivated again 
            if (DestroyUmbrella.DestroyedSprite == true)
            {
                PressF.SetActive(true);
                if (Input.GetKey(KeyCode.F))
                {
                    Shield.SetActive(true);
                    PressF.SetActive(false);
                    DestroyUmbrella.DestroyedSprite = false;
                }
            }
        }
    }
}

