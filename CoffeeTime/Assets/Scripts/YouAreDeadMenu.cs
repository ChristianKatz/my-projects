﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoLA
{
    public class YouAreDeadMenu : MonoBehaviour
    {
        //String Value for the scene name
        [SerializeField]
        private string ScenenameForTheMenu;

        private void Update()
        {
            // When the menu is called, the game should stop in order not to falsified the time highscore
            Time.timeScale = 0;

            //When the menu is called, the player should be able to see and move the cursor
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        // Method for a button to get back to the menu
        // If the button is pressed, the time should be reactivated
        public void Menu()
        {
            SceneManager.LoadScene(ScenenameForTheMenu);
            Time.timeScale = 1;
        }

        // Method for a button to try the level again
        // If the button is pressed, the time should be reactivated
        public void TryAgain()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Time.timeScale = 1;
        }
    }
}
