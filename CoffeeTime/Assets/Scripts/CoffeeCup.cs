﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoLA
{
    public class CoffeeCup : MonoBehaviour
    {
        // string Value for the scene name
        [SerializeField]
        private string ScenenameForMenu;

        // if the player hits the collider the menu will be opened
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                SceneManager.LoadScene(ScenenameForMenu);
            }
        }
    }
}
