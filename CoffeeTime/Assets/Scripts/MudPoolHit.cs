﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class MudPoolHit : MonoBehaviour
    {
        //I need this script, so that the player loses his health
        private PlayerMovement Player;

        // need the puddle as a GameObject to destroy it after the player touches it
        [SerializeField]
        private GameObject MudPool;

        void Start()
        {
            // calling up the script
            Player = FindObjectOfType<PlayerMovement>();
        }

        // use triggers, so that the player got damage when he touches the puddle
        // 1 life of 4 will be deducted, then the puddle will be destroyed
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                Player.Health--;
                Destroy(MudPool);
            }
        }
    }
}