﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// that is for the scene change
public class BlackFading : MonoBehaviour
{
    // animator, which contains the fade
    [SerializeField]
    private Animator animator;

    // string value, which has the sceneName
    [SerializeField]
    private string SceneName;
  
    // if the player walks into the trigger, a black screen will slowly appear and disappear to have a smooth scene change
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            animator.SetTrigger("NextLevel");         
        }
    }

    // when the fade is completed the next scene will appear
    public void OnFadeComplete()
    {
        SceneManager.LoadScene(SceneName);
    }
}
