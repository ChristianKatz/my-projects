﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activateItem1 : MonoBehaviour
{
    public GameObject pressF;
    public AudioSource Voice;
    public bool SecondLetterIsActivated;
    public GameObject Letter;
    
    private float Distance;
    public Transform Player;   
    public GameObject MoveUp;
    public GameObject MoveDown;
    public GameObject checkDistance;
    
    void Start()
    {
        pressF.SetActive(false);
        Voice.gameObject.SetActive(false);        
        Letter.SetActive(false);
        SecondLetterIsActivated = false;
    }

    void Update()
    {
        Distance = transform.position.z - Player.position.z;
        if (Distance > -10)
        {
            checkDistance.transform.position = Vector3.MoveTowards(checkDistance.transform.position, MoveUp.transform.position, -Distance * Time.deltaTime / 4);
        }
        if (Distance < -10)
        {
            checkDistance.transform.position = Vector3.MoveTowards(checkDistance.transform.position, MoveDown.transform.position, -Distance * Time.deltaTime / 6);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        pressF.SetActive(true);
    }

    private void OnTriggerStay(Collider other)
    {                  
            if (Input.GetKey(KeyCode.F))
            {               
                Voice.gameObject.SetActive(true);
                pressF.SetActive(false);
                Letter.SetActive(true);
                SecondLetterIsActivated = true;
            }                                                         
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.name =="Player")
        {
            Voice.gameObject.SetActive(false);
            pressF.SetActive(false);
            
        }        
    }
}
