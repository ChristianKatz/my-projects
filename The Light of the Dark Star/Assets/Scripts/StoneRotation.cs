﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// that is the script for the debris
public class StoneRotation : MonoBehaviour
{
    // contain the debris
    public GameObject[] Stones;

    // the rotation speed of the debris
    public float RotationSpeed;

    // i dont know
    public Vector3 Axis;
    public Vector3 Point;
    public float angles;


    // continuously rotates the debris 
    void Update()
    {
        Stones[0].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[0].transform.RotateAround(Point, Axis, angles);

        Stones[1].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[1].transform.RotateAround(Point, Axis, angles);

        Stones[2].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[2].transform.RotateAround(Point, Axis, angles);

        Stones[3].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[3].transform.RotateAround(Point, Axis, angles);

        Stones[4].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[4].transform.RotateAround(Point, Axis, angles);

        Stones[5].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[5].transform.RotateAround(Point, Axis, angles);

        Stones[6].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[6].transform.RotateAround(Point, Axis, angles);

        Stones[7].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[7].transform.RotateAround(Point, Axis, angles);

        Stones[8].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[8].transform.RotateAround(Point, Axis, angles);

        Stones[9].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[9].transform.RotateAround(Point, Axis, angles);

        Stones[10].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[10].transform.RotateAround(Point, Axis, angles);

        Stones[11].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[11].transform.RotateAround(Point, Axis, angles);

        Stones[12].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[12].transform.RotateAround(Point, Axis, angles);

        Stones[13].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[13].transform.RotateAround(Point, Axis, angles);

        Stones[14].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[14].transform.RotateAround(Point, Axis, angles);

        Stones[15].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[15].transform.RotateAround(Point, Axis, angles);

        Stones[16].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[16].transform.RotateAround(Point, Axis, angles);

        Stones[17].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[17].transform.RotateAround(Point, Axis, angles);

        Stones[18].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[18].transform.RotateAround(Point, Axis, angles);

        Stones[19].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[19].transform.RotateAround(Point, Axis, angles);

        Stones[20].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[20].transform.RotateAround(Point, Axis, angles);

        Stones[21].transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime, RotationSpeed * Time.deltaTime));
        Stones[21].transform.RotateAround(Point, Axis, angles);             
    }
}
