﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// that is the interaction with letters in the first level
public class ItemInteraction : MonoBehaviour
{
    // the particle system will be deactivated when the player interacts with the item
    [SerializeField]
    private GameObject PartikclSystem;

    // press f text for the player
    public GameObject PressF;

    // i don't knoiw
    public GameObject Text;

    // The letter prefab to activated or deactivated it
    [SerializeField]
    private GameObject Letter;

    // i don't know
    private bool TextisActivated;

    // if necessary I can add sound
    [SerializeField]
    private AudioSource audioSource;

    // need this script to change the cursor of the player
    private PlayerMovement playerMovement;

    // Cursor texture for the change
    [SerializeField]
    private Texture2D CursorTexture;

    // i don't know
    private bool deactivateText;

    // activates the letter in the inventory
    private bool activateLetter;
    public bool ActivateLetter
    {
        get
        {
            return activateLetter;
        }
        set
        {
            activateLetter = value;
        }
    }
 

    void Start()
    {
        // get the script
        playerMovement = FindObjectOfType<PlayerMovement>();

        // the player should only see the press f when he is in the trigger box
        PressF.SetActive(false);

        // the text is only active, when the player presses the f button in the trigger box
        Text.SetActive(false);

        // I don't know
        TextisActivated = false;

        // audio source is only active when the player presses the f button
        audioSource.gameObject.SetActive(false);
        
        // the letter is only available in the inventory when the player reads the letter
        activateLetter = false;

        // I don't know
        deactivateText = false;                     
    }

    // I don't know
    IEnumerator DeactivatingText()
    {
        yield return new WaitForSeconds(0.2f);
        deactivateText = true;
    }
    // i don't know
    IEnumerator reactivatingText()
    {
        yield return new WaitForSeconds(0.2f);
        deactivateText = false;
    }

    // when the player stepps in the trigger box he is able to see that he can press the f button
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            PressF.SetActive(true);
        }
    }

    // if the player stays in the trigger box and press the f button he can read the letter
    // audio could be played
    // cursor is deactivated because otherwise it would disturb the reading
    // particle system is deactivated
    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKey(KeyCode.F))
        {
            Text.SetActive(true);
            TextisActivated = true;
            audioSource.gameObject.SetActive(true);
            PressF.SetActive(false);
            activateLetter = true;
            Cursor.visible = false;
            StartCoroutine(DeactivatingText());
            PartikclSystem.SetActive(false);                       
        }
        // I think that this part was unecessary becasue of the changes
        // cursor is visible again
        // the text will disappear
        // letter will disappear
        if (Input.GetKey(KeyCode.F) && deactivateText == true)
        {          
            Text.SetActive(false);
            TextisActivated = false;
            audioSource.gameObject.SetActive(false);
            Cursor.visible = true;
            StartCoroutine(reactivatingText());
            Letter.SetActive(false);
        }
    }

    // if the player reads the letter and goes out the trigger box the text will be deactivated
    // the cursor is visible again
    // something is not clear
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            PressF.SetActive(false);
            Text.SetActive(false);
            TextisActivated = false;
            audioSource.gameObject.SetActive(false);
            Cursor.visible = true;

            if(activateLetter == true)
            {
                Letter.SetActive(false);
            }
        }
    }

    // if the player points on the letter the new cursor texture will appear
    private void OnMouseEnter()
    {
        Cursor.SetCursor(CursorTexture, new Vector2(0, 0), CursorMode.Auto);
    }
    // if the player leaves the letter with the cursor the old texture will appear again
    private void OnMouseExit()
    {
        Cursor.SetCursor(playerMovement.CursorTexture, new Vector2(0, 0), CursorMode.Auto);
    }
}
  
