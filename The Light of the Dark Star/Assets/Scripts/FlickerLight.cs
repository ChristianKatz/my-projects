﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// generate the flicker light
public class FlickerLight : MonoBehaviour
{
    // the light variable to modify the intensity
    private Light light;

    void Start()
    {
        // get the light component
        light = GetComponent<Light>();

        // start the coroutine when the game starts
        StartCoroutine(Lightcontrol());
    }

    // the light will flicker
    IEnumerator Lightcontrol()
    {               
            yield return new WaitForSeconds(Random.Range(0.1f, 0.8f));
            light.intensity = 0;
            yield return new WaitForSeconds(Random.Range(0.1f, 0.8f));
            light.intensity = 5;
            StartCoroutine(Lightcontrol());        
    }
}
